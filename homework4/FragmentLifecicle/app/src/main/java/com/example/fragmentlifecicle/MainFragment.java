package com.example.fragmentlifecicle;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class MainFragment extends Fragment {

    private static final String tag = "FragmentLifecicle";

    public static MainFragment newInstance() {

        Bundle args = new Bundle();

        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        showToast("onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showToast("onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        showToast("onCreateView");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        showToast("onActivityCreated");
    }

    @Override
    public void onStart() {
        super.onStart();
        showToast("onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        showToast("onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        showToast("onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        showToast("onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        showToast("onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        showToast("onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        showToast("onDetach");
    }

    private void showToast(String str) {
        Toast.makeText(getActivity(), str, Toast.LENGTH_LONG).show();
        Log.i(tag, str);
    }
}
