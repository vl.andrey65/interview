package ru.geekbrains.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.Toast;

public class MyWidget extends AppWidgetProvider {

    private final static String ExtraMsg = "msg";
    private final static String ACTION_WIDGET_RECEIVER = "ACTION_WIDGET_RECEIVER";

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        CharSequence widgetText = context.getString(R.string.appwidget_text);
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.my_widget);
        // Делаем обработку клика кнопки
        Intent intent = new Intent(context, MyWidget.class);
        intent.setAction(ACTION_WIDGET_RECEIVER);
        intent.putExtra(ExtraMsg, "Button clicked!");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        views.setOnClickPendingIntent(R.id.button, pendingIntent);
        // Здесь обновим текст, будем показывать номер виджета
        views.setTextViewText(R.id.appwidget_text, String.format("%s - %d", widgetText, appWidgetId));
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        // Ловим broadcast от нажатия кнопки
        final String action = intent.getAction();
        if (action.equals(ACTION_WIDGET_RECEIVER)) {
            String message = intent.getStringExtra(ExtraMsg);
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }
}

