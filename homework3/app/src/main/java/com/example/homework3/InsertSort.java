package com.example.homework3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InsertSort {

    public static void main(String[] args) {
        // сортировка вставками
        List<Integer> arr = Arrays.asList(1,9,3,7,2,4,8);
        insertSort(arr);
        System.out.println(arr);

        // ряд фибоначи, сложность O(n)
        System.out.println(fibonachi(15));
    }

    static void insertSort(List<Integer> arr) {
        for (int i = 1; i < arr.size(); i++) {
            int temp = arr.get(i);
            int j = i;

            while (j > 0 && temp <= arr.get(j - 1)) {
                int cur = arr.get(j);
                int prev = arr.get(j - 1);
                arr.set(j, prev);
                arr.set(j - 1, cur);
                --j;
            }
        }
    }

    static ArrayList<Integer> fibonachi(int count) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            if (i == 0 || i == 1) {
                result.add(i);
                continue;
            }
            int prev1 = result.get(i - 1);
            int prev2 = result.get(i - 2);
            result.add(prev1 + prev2);
        }
        return result;
    }

}
