package com.example.myapplication;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ConverterTest {

    @Test
    public void convertTest() {
        ConvertTo convertTo = mock(ConvertTo.class);
        float source = 60;
        Converter converter = new Converter(source);
        converter.convert(convertTo);
        verify(convertTo).doConvert(60);
    }

    @Test
    public void convertResult() {
        float source = 60;
        Converter converter = new Converter(60);
        converter.convert(new SpeedConverter());
        float result = Math.round(converter.result());
        assertThat(result, is(17f));
    }

}
