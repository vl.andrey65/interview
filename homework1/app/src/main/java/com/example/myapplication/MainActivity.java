package com.example.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText mEditText;
    private Button mButton;
    private TextView resultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUi();
    }

    private void initUi() {
        mEditText = findViewById(R.id.editText);
        mButton = findViewById(R.id.button);
        resultTextView = findViewById(R.id.result);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Converter converter = new Converter(Float.parseFloat(mEditText.getText().toString()));
                converter.convert(new SpeedConverter());
                resultTextView.setText(String.format("%.02f", converter.result()));
            }
        });
    }
}
