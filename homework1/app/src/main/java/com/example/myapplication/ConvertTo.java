package com.example.myapplication;

public interface ConvertTo {

    float doConvert(float source);

}
