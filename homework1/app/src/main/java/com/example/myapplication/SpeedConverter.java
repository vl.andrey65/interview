package com.example.myapplication;

public class SpeedConverter implements ConvertTo {

    @Override
    public float doConvert(float source) {
        return source * 1000 / 3600;
    }
}
