package com.example.myapplication;

class Converter {
    private float sourceValue;
    private float destValue;

    Converter(float sourceValue){
        this.sourceValue = sourceValue;
    }

    Converter convert(ConvertTo convertTo){
        destValue = convertTo.doConvert(sourceValue);
        return  this;
    }

    float result(){
        return destValue;
    }
}
